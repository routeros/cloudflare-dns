################# CloudFlare variables #################
:global WANInterface "ether1";

:local CFemail "user@example.com";
:local CFtkn "daa3e9e7377c5be300e9c0debdb9186544ed7"; # Your CloudFlare API key comes here

:local CFrecordType "A";
:local CFzoneid "def29c2ac991ae5e8e825a616f7a5f41"; # Your DNS zone ID comes here (a hash of 32 chars)

# An associative array of domain names to domainIds (32 chars each, you'll need to query CF to get them)
:local CFdomains { "router.example.com"="34a8e66fca93b25703cce4f17579b918" };
# An associative array of domain names to whether you want to enable CF proxying or not
:local CFproxied { "router.example.com"="false" };
# You'll need to have at least one domain that's proxied=false, otherwise resolve will keep getting CF's proxy IP instead of yours
:local CFunproxiedDomain "router.example.com";

#########################################################################
########################  DO NOT EDIT BELOW  ############################
#########################################################################

################# Internal variables #################
:local resolvedIP "";
:global WANip "";

################# Resolve and set IP-variables #################
:local currentIP [/ip address get [/ip address find interface=$WANInterface ] address];
:set WANip [:pick $currentIP 0 [:find $currentIP "/"]];
:set resolvedIP [:resolve $CFunproxiedDomain];

######## Compare and update CF if necessary #####
:if ($resolvedIP != $WANip) do={
  :log info "cloudflare: $CFunproxiedDomain $resolvedIP => $WANip";
  :foreach domainName,domainId in=$CFdomains do={
    :local proxied ($CFproxied->$domainName);
    :local url "https://api.cloudflare.com/client/v4/zones/$CFzoneid/dns_records/$domainId";
    :local header "X-Auth-Email:$CFemail,X-Auth-Key:$CFtkn,content-type:application/json";
    :local data "{\"type\":\"$CFrecordType\",\"name\":\"$domainName\",\"content\":\"$WANip\",\"proxied\":$proxied}";
    :local result [ /tool fetch http-method=put mode=https url=$url http-header-field=$header http-data=$data output=user as-value ];
    :if ([len $result] > 1) do={
      :log info "cloudflare: $result";
    }
  };
} else={
  :log info "cloudflare: no update needed"
};
